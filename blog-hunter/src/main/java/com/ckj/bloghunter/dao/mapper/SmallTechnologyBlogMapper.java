package com.ckj.bloghunter.dao.mapper;

import com.ckj.bloghunter.dao.dataobject.SmallTechnologyBlog;
import com.ckj.bloghunter.dao.util.MyMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

public interface SmallTechnologyBlogMapper extends MyMapper<SmallTechnologyBlog> {
}
package com.ckj.bloghunter.domain.service.common;

import java.io.Serializable;

public interface KeyValueEnum extends Serializable {

    Integer getCode();

    String getDesc();
}

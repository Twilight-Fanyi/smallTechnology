package com.ckj.bloghunter.domain.service.common;

/**
 * @author c.kj
 * @Description
 * @Date 2019-12-19
 * @Time 18:03
 **/
public enum ShowEnum implements KeyValueEnum {

    YES(1, "Yes"),
    NO(0, "No");

    private Integer code;

    private String  desc;

    ShowEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }

}

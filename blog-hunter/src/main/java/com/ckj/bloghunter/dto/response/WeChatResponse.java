package com.ckj.bloghunter.dto.response;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author c.kj
 * @Description
 * @Date 2020-01-08
 * @Time 19:05
 *
 **/
@Data
public class WeChatResponse implements Serializable {

    private static final long         serialVersionUID = -3215090123894861218L;

    private Integer errcode;

    private String errmsg;

    private List<String> id_list;

    private String access_token;

    private Long expires_in;



}

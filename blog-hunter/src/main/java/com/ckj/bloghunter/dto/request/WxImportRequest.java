package com.ckj.bloghunter.dto.request;

import java.io.Serializable;

import lombok.Data;

/**
 * @author c.kj
 * @Description
 * @Date 2019-12-19
 * @Time 19:14
 **/
@Data
public class WxImportRequest implements Serializable{

    private static final long serialVersionUID = -3215090123894869218L;


}

// 云函数入口文件
const cloud = require('wx-server-sdk');
cloud.init({ env: process.env.Env });
const db = cloud.database();
const _ = db.command;

// received a notification
const template = 'cwYd6eGpQ8y7xcVsYWuTSC-FAsAyv5KOAVGvjJIdI9Q';

cloud.init();

// cloud function entry function
exports.main = async (event) => {
    switch (event.action) {
        case 'sendTemplateMessage': {
            return sendTemplateMessage(event)
        }
        case 'removeExpireFormId': {
            return removeExpireFormId(event)
        }
        case 'queryFormIds': {
            return queryFormIds(event)
        }
        case 'addFormIds': {
            return addFormIds(event)
        }
        default:
            break
    }
};


/**
 * send notification message
 * @param  event
 */
async function sendTemplateMessage(event) {

    var touser;
    var form_id;
    var openId = event.tOpenId === "" ? process.env.author : event.tOpenId;
    var openIdformIds = await db.collection('smallTechnology_formIds').where({
        openId: openId
    }).limit(1).get()
    if (openIdformIds.code) {
        return;
    }
    if (!openIdformIds.data.length) {
        return;
    }
    touser = openIdformIds.data[0]['openId'];
    form_id = openIdformIds.data[0]['formId'];
    console.info("openId:" + touser + ";formId:" + form_id);
    console.info(event.nickName + ":" + event.message)
    return await cloud.openapi.templateMessage.send({
        touser: touser,
        templateId: template,
        formId: form_id,
        page: 'pages/detail/detail?id=' + event.blogId,
        data: {
            keyword1: {
                value: event.nickName
            },
            keyword2: {
                value: event.message
            },
            keyword3: {
                value: new Date().toFormat("YYYY-MM-DD HH24:MI:SS")
            }
        },
    })
}

/**
 * get total formids and expired formids
 * @param {} event
 */
async function queryFormIds(event) {
    var data = {};
    var formIdsResult = await db.collection('smallTechnology_formIds').where({
        openId: process.env.author // 填入当前用户 openid
    }).count();
    data.formIds = formIdsResult.total;
    return data;
}

/**
 * delete expired formid
 * @param {} event
 */
async function removeExpireFormId(event) {
    try {
        return await db.collection('smallTechnology_formIds').where({
            timestamp: _.lt(new Date().removeDays(7).getTime())
        }).remove()
    } catch (e) {
        console.error(e)
    }
}

/**
 * add formid
 * @param {} event
 */
async function addFormIds(event) {
    try {
        let removeRes = await db.collection('smallTechnology_formIds').where({
            timestamp: _.lt(new Date().removeDays(7).getTime())
        }).remove();
        console.info(removeRes);
        for (var i = 0; i < event.formIds.length; i++) {
            let data = {
                openId: event.userInfo.openId,
                formId: event.formIds[i],
                timestamp: new Date().getTime()
            };
            let res = await db.collection('smallTechnology_formIds').add({
                data: data
            });
            console.info(res)
        }
        return true;
    } catch (e) {
        console.error(e);
        return false;
    }
}

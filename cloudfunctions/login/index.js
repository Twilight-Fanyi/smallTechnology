// 云函数模板

const cloud = require('wx-server-sdk')

// 初始化 cloud
cloud.init();

exports.main = (event, context) => {
    console.log(event);
    console.log(context);
    const wxContext = cloud.getWXContext();
    return {
        event,
        openid: wxContext.OPENID,
        appid: wxContext.APPID,
        unionid: wxContext.UNIONID,
    }
};
